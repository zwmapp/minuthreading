package com.example.administrator.minuthreading.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Administrator on 11/20/2017.
 */

public class Service {

    @SerializedName("servicemsg")
    @Expose
    public String serviceMsg;
    @SerializedName("allservices")
    @Expose
    public ArrayList<ServiceItems> allservices;


    @Override
    public String toString() {
        return "Service{" +
                "serviceMsg='" + serviceMsg + '\'' +
                ", allservices=" + allservices +
                '}';
    }


    public Service(String serviceMsg, ArrayList<ServiceItems> allservices) {
        this.serviceMsg = serviceMsg;
        this.allservices = allservices;
    }



}

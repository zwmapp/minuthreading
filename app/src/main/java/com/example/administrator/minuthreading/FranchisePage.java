package com.example.administrator.minuthreading;

import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.administrator.minuthreading.Util.Api;
import com.example.administrator.minuthreading.fragments.AppBar;
import com.example.administrator.minuthreading.model.Franchise;

import org.json.JSONException;
import org.json.JSONObject;

public class FranchisePage extends AppCompatActivity {
    Franchise franchise;
    TextView tv_franchise;
    public EditText fname,lname,email,tele,mobile,aoi,addInfo;
    public Button bt_submit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_franchise_page);

        AppBar mainAppBar = (AppBar) getSupportFragmentManager().findFragmentById(R.id.mainAppBar);
        mainAppBar.setAppbarTitle("FRANCHISE");
        mainAppBar.setBackEnabled(true);
        tv_franchise=(TextView)findViewById(R.id.franchise_tv);
        franchiseFetch();
        fname=(EditText)findViewById(R.id.franchise_fname_et);
        lname=(EditText)findViewById(R.id.franchise_lname_et);
        email=(EditText)findViewById(R.id.franchise_email_et);
        tele=(EditText)findViewById(R.id.franchise_tele_et);
        mobile=(EditText)findViewById(R.id.franchise_mobile_et);
        aoi=(EditText)findViewById(R.id.franchise_aoi_et);
        addInfo=(EditText)findViewById(R.id.franchise_info_et);
        bt_submit=(Button)findViewById(R.id.franchise_submit_bt);

        bt_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Validation()) {
                    String firstName = fname.getText().toString().trim();
                    String lastName = lname.getText().toString().trim();
                    String emailAdd = email.getText().toString().trim();
                    String telephone = tele.getText().toString().trim();
                    String mobileNo = mobile.getText().toString().trim();
                    String aoInterest = aoi.getText().toString().trim();
                    String addInformation = addInfo.getText().toString().trim();
                    franchiseSubmit(firstName, lastName, emailAdd, telephone, mobileNo, aoInterest, addInformation);
                }
            }
        });
    }

    public void franchiseFetch(){
        final ProgressDialog loading = ProgressDialog.show(FranchisePage.this, "Loading", "Please wait", false, false);
        StringRequest stringRequest=new StringRequest(Request.Method.POST, Api.URL_FRANCHISE_DESC, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonObject=new JSONObject(response);
                        if (jsonObject.getBoolean("status")==true){
                            tv_franchise.setText(Html.fromHtml(jsonObject.getString("items")));
                             Log.e("FranchiseText :",jsonObject.getString("items"));
                        }else{
                            Toast.makeText(FranchisePage.this, jsonObject.getString("msg"), Toast.LENGTH_SHORT).show();
                        }
                    loading.dismiss();

                } catch (JSONException e) {
                    loading.dismiss();
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                loading.dismiss();
            }
        });

        RequestQueue requestQueue= Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    public void franchiseSubmit(String firstName,String lastName,String emailAdd,String telephoneNo,String mobileNo,String aOfi,String addInformation ){

        String url=Api.URL_FRANCHISE_FORM+"&text-65="+firstName+"&text-913="+lastName+"&email-860="+emailAdd+"&tel-717="+telephoneNo+"&tel-424="+mobileNo+"&text-94="+aOfi+"&text-457="+addInformation;
        Log.e("FranchiseUrl :",url);
        final ProgressDialog loading = ProgressDialog.show(FranchisePage.this, "Loading", "Please wait", false, false);
        StringRequest stringRequest=new StringRequest(Request.Method.POST,url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonObject=new JSONObject(response);
                    if (jsonObject.getBoolean("status")==true){

                        fname.getText().clear();
                        lname.getText().clear();
                        email.getText().clear();
                        tele.getText().clear();
                        mobile.getText().clear();
                        aoi.getText().clear();
                        addInfo.getText().clear();

                        Toast.makeText(FranchisePage.this, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();

                    }else{
                        Toast.makeText(FranchisePage.this, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                    }
                    loading.dismiss();

                } catch (JSONException e) {
                    loading.dismiss();
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                loading.dismiss();
            }
        });

        RequestQueue requestQueue= Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    public boolean Validation(){
        if( fname.getText().toString().length() == 0 ) {
            Toast.makeText(FranchisePage.this,"First Name is required!",Toast.LENGTH_SHORT).show();
            return false;
        }else if( lname.getText().toString().length() == 0 ) {
            Toast.makeText(FranchisePage.this,"Last name is required!",Toast.LENGTH_SHORT).show();
            return false;
        }else if( tele.getText().toString().length() == 0  ) {
            Toast.makeText(FranchisePage.this,"Telephone Number  is required!",Toast.LENGTH_SHORT).show();
            return false;
        }else if( mobile.getText().toString().length() ==0) {
            Toast.makeText(FranchisePage.this,"Mobile Number  is required!",Toast.LENGTH_SHORT).show();
            return false;
        }else if( email.getText().toString().length() == 0) {
            Toast.makeText(FranchisePage.this,"Email is required!",Toast.LENGTH_SHORT).show();
            return false;
        }else if( !email.getText().toString().matches("[a-zA-Z0-9._-]+@[a-z]+.[a-z]+") ) {
            Toast.makeText(FranchisePage.this,"Correct Email Address Required!",Toast.LENGTH_SHORT).show();
            return false;
        }else if((aoi.getText().toString().length() == 0)) {
            Toast.makeText(FranchisePage.this,"Please gives Area ofInterest",Toast.LENGTH_SHORT).show();
            return false;
        }else if((addInfo.getText().toString().length() == 0)) {
            Toast.makeText(FranchisePage.this,"Please gives some Additional Information",Toast.LENGTH_SHORT).show();
            return false;
        }else{
            return true;
        }
    }
}

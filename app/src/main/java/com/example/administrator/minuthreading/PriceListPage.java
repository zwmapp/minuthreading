package com.example.administrator.minuthreading;

import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ExpandableListView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.administrator.minuthreading.Adapter.PriceListAdapter;
import com.example.administrator.minuthreading.Util.Api;
import com.example.administrator.minuthreading.fragments.AppBar;
import com.example.administrator.minuthreading.model.Price;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;

public class PriceListPage extends AppCompatActivity {


    ArrayList<Price> pricelist=null;
    ExpandableListView elv_price;
    PriceListAdapter adapter=null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_price_list_page);
        elv_price=(ExpandableListView)findViewById(R.id.priceElv);
        elv_price.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
            int previousGroup = -1;

            @Override
            public void onGroupExpand(int groupPosition) {
                if(groupPosition != previousGroup)
                    elv_price.collapseGroup(previousGroup);
                previousGroup = groupPosition;
            }
        });

        pricelist();

        AppBar mainAppBar = (AppBar) getSupportFragmentManager().findFragmentById(R.id.mainAppBar);
        mainAppBar.setAppbarTitle("PRICE LIST");
        mainAppBar.setBackEnabled(true);
    }

    public void pricelist(){
        final ProgressDialog loading = ProgressDialog.show(PriceListPage.this, "", "", false, false);
        JsonObjectRequest objectRequest= new JsonObjectRequest(Request.Method.POST, Api.URL_PRICELIST, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                Gson gson=new Gson();
                Type type= new TypeToken<ArrayList<Price>>(){}.getType();
                try {

                    pricelist=gson.fromJson(response.getString("items"),type);

                    } catch (JSONException e) {
                    e.printStackTrace();
                }
                adapter=new PriceListAdapter(PriceListPage.this,pricelist);
                elv_price.setAdapter(adapter);
                loading.dismiss();
                Log.e("PriceList",pricelist.toString());
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });

        RequestQueue requestQueue= Volley.newRequestQueue(PriceListPage.this);
        requestQueue.getCache().clear();
        requestQueue.add(objectRequest);

    }
}

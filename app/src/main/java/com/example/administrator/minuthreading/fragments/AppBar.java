package com.example.administrator.minuthreading.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;


import com.example.administrator.minuthreading.R;

/**
 * Created by Administrator on 11/16/2017.
 */

public class AppBar extends Fragment {
    private Toolbar toolbar = null;
    private AppCompatActivity activity = null;
    private ActionBar actionBar = null;

    public AppBar() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        activity = (AppCompatActivity) getActivity();
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_app_bar, container, false);

        toolbar = (Toolbar) view.findViewById(R.id.toolbar);
        activity.setSupportActionBar(toolbar);
        actionBar = activity.getSupportActionBar();


        setHasOptionsMenu(true);
        return view;
    }

    public void setAppbarTitle(String title){
        if(null != actionBar){
            actionBar.setTitle(title);


        }
    }

    public void setBackEnabled(Boolean option){
        if(null != actionBar){
            actionBar.setDisplayHomeAsUpEnabled(option);
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        //super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.main, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        //return super.onOptionsItemSelected(item);

        switch (item.getItemId()){
            case android.R.id.home:
                getActivity().finish();
                return true;
            /*case R.id.actionCall:
                if (isPermissionGranted()) {
                    call_action();
                }
                return true;
            case R.id.home_page:
                startActivity(new Intent(getActivity(), MainActivity.class));
                return true;
            case R.id.about_us_page:
                startActivity(new Intent(getActivity(), AboutUsScreen.class));
                return true;
            case R.id.contact_us_page:
                startActivity(new Intent(getActivity(), ContactUsScreen.class));
                return true;
            case R.id.free_quote_page:
                startActivity(new Intent(getActivity(), FreeQuoteScreen.class));
                return true;
            case R.id.gallery_page:
                startActivity(new Intent(getActivity(), GalleryScreen.class));
                return  true;
            case R.id.testinomials_page:
                startActivity(new Intent(getActivity(), TestmonialsScreen.class));
                return  true;

*/
            default:
                return false;
        }

    }






}

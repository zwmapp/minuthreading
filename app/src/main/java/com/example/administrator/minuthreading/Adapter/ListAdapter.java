package com.example.administrator.minuthreading.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.administrator.minuthreading.R;
import com.example.administrator.minuthreading.model.Price;

import java.util.ArrayList;

/**
 * Created by Administrator on 11/20/2017.
 */

public class ListAdapter extends BaseAdapter {

    LayoutInflater inflater;
    Context context;
    ArrayList<ArrayList<String>> price;
    Price pricelist;


    public ListAdapter(ArrayList<ArrayList<String>> priceitems) {
        this.context=context;
        this.price=priceitems;
        this.inflater=LayoutInflater.from(this.context);
    }



    @Override
    public int getCount() {
        return price.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        MyViewHolder mViewHolder;

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.pricelist_list_items, parent, false);
            mViewHolder = new MyViewHolder(convertView);
            convertView.setTag(mViewHolder);
        } else {
            mViewHolder = (MyViewHolder) convertView.getTag();
        }
            for(int i=0;i<pricelist.priceitems.size();i++){
                mViewHolder.tvTitle.setText(pricelist.priceitems.get(0).toString());
                mViewHolder.tvPrice.setText(pricelist.priceitems.get(1).toString());
            }

        return convertView;


    }

    private class MyViewHolder {
        TextView tvTitle, tvPrice;


        public MyViewHolder(View item) {
            tvTitle = (TextView) item.findViewById(R.id.price_items);
            tvPrice = (TextView) item.findViewById(R.id.price_tv);

        }
    }
}

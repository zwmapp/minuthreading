package com.example.administrator.minuthreading.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Administrator on 11/17/2017.
 */

public class HomePage {

    @SerializedName("slider")
    @Expose()
    public ArrayList<String> bannerUrls;

    @SerializedName("welcometext")
    @Expose()
    public  String welcomeText;

    @SerializedName("video")
    @Expose
    public String videoUrl;



    public HomePage(ArrayList<String> bannerUrls, String welcomeText, String videoUrl) {
        this.bannerUrls = bannerUrls;
        this.welcomeText = welcomeText;
        this.videoUrl = videoUrl;
    }


}

package com.example.administrator.minuthreading.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by amit on 5/11/2018.
 */

public class Location {

    @SerializedName("locationId")
    @Expose
    public String locationId;
    @SerializedName("locationAddress")
    @Expose
    public String locationAddress;
    @SerializedName("locationTimeTable")
    @Expose
    public String locationTimeTable;
    @SerializedName("locationPhone")
    @Expose
    public String locationPhone;

    public Location(String locationId, String locationAddress, String locationTimeTable, String locationPhone) {
        this.locationId = locationId;
        this.locationAddress = locationAddress;
        this.locationTimeTable = locationTimeTable;
        this.locationPhone = locationPhone;
    }

    public String getLocationId() {
        return locationId;
    }

    public void setLocationId(String locationId) {
        this.locationId = locationId;
    }

    public String getLocationAddress() {
        return locationAddress;
    }

    public void setLocationAddress(String locationAddress) {
        this.locationAddress = locationAddress;
    }

    public String getLocationTimeTable() {
        return locationTimeTable;
    }

    public void setLocationTimeTable(String locationTimeTable) {
        this.locationTimeTable = locationTimeTable;
    }

    public String getLocationPhone() {
        return locationPhone;
    }

    public void setLocationPhone(String locationPhone) {
        this.locationPhone = locationPhone;
    }

    @Override
    public String toString() {
        return "Location{" +
                "locationId='" + locationId + '\'' +
                ", locationAddress='" + locationAddress + '\'' +
                ", locationTimeTable='" + locationTimeTable + '\'' +
                ", locationPhone='" + locationPhone + '\'' +
                '}';
    }
}

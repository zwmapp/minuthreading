package com.example.administrator.minuthreading;

import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.administrator.minuthreading.Adapter.LocationAdapter;
import com.example.administrator.minuthreading.Util.Api;
import com.example.administrator.minuthreading.fragments.AppBar;
import com.example.administrator.minuthreading.model.Location;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;

public class LocationPage extends AppCompatActivity {



    public ArrayList<Location> location;

    public RecyclerView recyclerView;
    public LocationAdapter locationAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location_page);


        AppBar mainAppBar = (AppBar) getSupportFragmentManager().findFragmentById(R.id.mainAppBar);
        mainAppBar.setAppbarTitle("LOCATION");
        mainAppBar.setBackEnabled(true);

        recyclerView=(RecyclerView)findViewById(R.id.location_rv);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setHasFixedSize(true);

        locationFetch();
    }

    public void locationFetch(){
        final ProgressDialog loading = ProgressDialog.show(LocationPage.this, "", "", false, false);
        StringRequest stringRequest=new StringRequest(Request.Method.POST, Api.URL_LOCATION, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                Gson gson=new Gson();

                try {
                    JSONObject jsonObject=new JSONObject(response);
                    JSONArray jsonArray=jsonObject.getJSONArray("items");
                    Type type= new TypeToken<ArrayList<Location>>(){}.getType();
                    location=gson.fromJson(String.valueOf(jsonArray),type);
                    loading.dismiss();
                    Log.e("Location",location.toString());
                    location.toString();
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                locationAdapter=new LocationAdapter(LocationPage.this,location);
                recyclerView.setAdapter(locationAdapter);



            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });

        RequestQueue requestQueue= Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }
}

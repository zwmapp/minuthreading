package com.example.administrator.minuthreading.Adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.administrator.minuthreading.PriceListPage;
import com.example.administrator.minuthreading.R;
import com.example.administrator.minuthreading.model.Price;

import java.util.ArrayList;

/**
 * Created by Administrator on 11/30/2017.
 */

public class PriceListAdapter extends BaseExpandableListAdapter {

    Context context;
    ArrayList<Price> priceList;

    public PriceListAdapter(PriceListPage pricelist, ArrayList<Price> priceList) {
        this.context=pricelist;
        this.priceList=priceList;
    }

    @Override
    public int getGroupCount() {
        return priceList.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return priceList.get(groupPosition).priceitems.size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return priceList.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return priceList.get(groupPosition).priceitems.get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {

        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this.context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.pricelist_card_items, null);
        }

        TextView priceTitle = (TextView) convertView.findViewById(R.id.price_title_tv);
        priceTitle.setText(priceList.get(groupPosition).title);

        return convertView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) this.context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = inflater.inflate(R.layout.pricelist_list_items, parent, false);

        // Get childrow.xml file elements and set values
        TextView itemTitle=(TextView)convertView.findViewById(R.id.price_items);
        itemTitle.setText(priceList.get(groupPosition).priceitems.get(childPosition).get(0).toString());
        TextView itemPrice=(TextView)convertView.findViewById(R.id.price_tv);
        itemPrice.setText(priceList.get(groupPosition).priceitems.get(childPosition).get(1).toString());


        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return false;
    }
}

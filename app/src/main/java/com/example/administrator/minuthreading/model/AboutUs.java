package com.example.administrator.minuthreading.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Administrator on 11/17/2017.
 */

public class AboutUs {

    @SerializedName("image")
    @Expose
    public String about_Url;

    @SerializedName("data")
    @Expose
    public String about_data;

    public AboutUs(String about_Url, String about_data) {
        this.about_Url = about_Url;
        this.about_data = about_data;
    }

    @Override
    public String toString() {
        return "AboutUs{" +
                "about_Url='" + about_Url + '\'' +
                ", about_data='" + about_data + '\'' +
                '}';
    }



}

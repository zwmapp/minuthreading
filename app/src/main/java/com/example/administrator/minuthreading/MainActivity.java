package com.example.administrator.minuthreading;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.text.Html;
import android.util.Log;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.example.administrator.minuthreading.Util.Api;
import com.example.administrator.minuthreading.model.HomePage;
import com.example.administrator.minuthreading.model.Testimonial;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerFragment;
import com.google.android.youtube.player.YouTubePlayerSupportFragment;
import com.google.android.youtube.player.YouTubePlayerView;
import com.google.gson.Gson;

import com.google.gson.reflect.TypeToken;
import com.synnapps.carouselview.CarouselView;
import com.synnapps.carouselview.ImageListener;
import com.synnapps.carouselview.ViewListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.example.administrator.minuthreading.Util.Config.DEVELOPER_KEY;

public class MainActivity extends AppCompatActivity  implements NavigationView.OnNavigationItemSelectedListener, ImageListener, YouTubePlayer.OnInitializedListener ,View.OnClickListener{

    HomePage homePageModel = null;
    ArrayList<Testimonial> testimonial = null;
    public TextView tv_welcomeText;
    CarouselView bannerView;
    CarouselView bottombanner;
    MyViewHolder holder;
    YouTubePlayerSupportFragment myYouTubePlayerFragment;
    private static final int RECOVERY_DIALOG_REQUEST = 1;
    private Button bt_readMore;
    String pattern = "(?<=watch\\?v=|/videos/|embed\\/)[^#\\&\\?]*";
    String youtubeCode;
    public WebView webview;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        testimonial = new ArrayList<>();
        tv_welcomeText = (TextView) findViewById(R.id.welcomeText_tv);
        bannerView = (CarouselView) findViewById(R.id.banner);
        webview = (WebView) findViewById(R.id.indexTouTube_wv);
        bt_readMore=(Button)findViewById(R.id.home_readMore_bt);
        bt_readMore.setOnClickListener(this);

        bottombanner = (CarouselView) findViewById(R.id.bottombanner);
        homeInfo();
        tesimonialInfo();





        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_services) {
            Intent g = new Intent(MainActivity.this, ServicesPage.class);
            startActivity(g);
        } else if (id == R.id.nav_price) {
            Intent g = new Intent(MainActivity.this, PriceListPage.class);
            startActivity(g);

        } else if (id == R.id.nav_appointment) {
            Intent a =new Intent(MainActivity.this ,AppointmentPage.class);
            startActivity(a);

        } else if (id == R.id.nav_career) {

            Intent c =new Intent(MainActivity.this ,CareerPage.class);
            startActivity(c);

        } else if (id == R.id.nav_about) {

            Intent c =new Intent(MainActivity.this ,AboutUsPage.class);
            startActivity(c);

        }else if (id == R.id.nav_location) {

            Intent c =new Intent(MainActivity.this ,LocationPage.class);
            startActivity(c);

        }else if (id == R.id.nav_franchise) {

            Intent c =new Intent(MainActivity.this ,FranchisePage.class);
            startActivity(c);

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void homeInfo() {
        final ProgressDialog loading = ProgressDialog.show(MainActivity.this, "", "", false, false);
        JsonObjectRequest stringRequest = new JsonObjectRequest(Request.Method.POST, Api.URL_HOME, new Response.Listener<JSONObject>() {
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
            @Override
            public void onResponse(JSONObject response) {
                Gson gson = new Gson();
                try {
                    homePageModel = gson.fromJson(response.getString("items"), HomePage.class);
                    if (homePageModel != null) {
                        initSlider();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                loading.dismiss();

              //  String URL=homePageModel.videoUrl;
                String URL="https://www.youtube.com/embed/q9Jymlf_5i8";
               // Log.e("Youtube",URL);

                webview.setWebViewClient(new WebViewClient());
                webview.clearCache(true);
                webview.clearHistory();
                webview.getSettings().setJavaScriptEnabled(true);
                webview.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
                webview.getSettings().setPluginState(WebSettings.PluginState.ON);
                webview.getSettings().setMediaPlaybackRequiresUserGesture(false);
                webview.loadUrl(URL);

              /*  myYouTubePlayerFragment = (YouTubePlayerSupportFragment)getSupportFragmentManager().findFragmentById(R.id.youtube_view);
                myYouTubePlayerFragment.initialize(DEVELOPER_KEY, MainActivity.this);*/
                Log.e("TAG", homePageModel.toString());

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });

        RequestQueue requestQueue = Volley.newRequestQueue(MainActivity.this);
        requestQueue.add(stringRequest);


    }

    private void initSlider() {
        tv_welcomeText.setText(homePageModel.welcomeText);
        bannerView.setImageListener(this);
        bannerView.setPageCount(homePageModel.bannerUrls.size());
    }

    @Override
    public void setImageForPosition(int position, ImageView imageView) {
        imageView.setScaleType(ImageView.ScaleType.FIT_XY);
        Glide.with(MainActivity.this)
                .load(homePageModel.bannerUrls.get(position))
                .into(imageView);
    }

    private void tesimonialInfo() {
        JsonObjectRequest jsonObject = new JsonObjectRequest(Request.Method.POST, Api.URL_TESTIMONIALS, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Type type = new TypeToken<ArrayList<Testimonial>>() {
                }.getType();
                Gson gson = new Gson();
                try {
                    testimonial = gson.fromJson(((JSONObject) response.getJSONObject("items")).getString("tetimonial"), type);
                    setupTestimonials();
                    Log.e("Testimonials", testimonial.toString());
                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        RequestQueue requestQueue = Volley.newRequestQueue(MainActivity.this);
        requestQueue.add(jsonObject);
    }

    private void setupTestimonials() {

        bottombanner.setViewListener(new ViewListener() {
            @Override
            public View setViewForPosition(int position) {
                View view = LayoutInflater.from(MainActivity.this).inflate(R.layout.testimonial_items, null, false);

                holder= new MyViewHolder(view);
                holder.tvDesc.setText(Html.fromHtml(testimonial.get(position).testDescp));
                if ((testimonial.get(position).imageURl)!=null)
                    Glide.with(MainActivity.this).load(testimonial.get(position).imageURl).into(holder.ivIcon);
                else
                    holder.ivIcon.setImageResource(R.drawable.user);

                return view;
            }
        });
        bottombanner.setPageCount(testimonial.size());
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){
            case R.id.home_readMore_bt:
                Intent i= new Intent(MainActivity.this, AboutUsPage.class);
                startActivity(i);
        }

    }

    private class MyViewHolder {
        TextView tvTitle, tvDesc;
        ImageView ivIcon;

        public MyViewHolder(View item) {

            tvDesc = (TextView) item.findViewById(R.id.user_tv);
            ivIcon = (ImageView) item.findViewById(R.id.user_iv);
        }
    }
    @Override
    public void onInitializationFailure(YouTubePlayer.Provider provider,
                                        YouTubeInitializationResult errorReason) {
        if (errorReason.isUserRecoverableError()) {
            errorReason.getErrorDialog(this, RECOVERY_DIALOG_REQUEST).show();
        } else {
            String errorMessage = String.format(
                    getString(R.string.error_player), errorReason.toString());
            Toast.makeText(this, errorMessage, Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onInitializationSuccess(YouTubePlayer.Provider provider,
                                        YouTubePlayer player, boolean wasRestored) {
        if (!wasRestored) {
            // loadVideo() will auto play video
            // Use cueVideo() method, if you don't want to play it automatically
            String URL=homePageModel.videoUrl;

            Pattern compiledPattern = Pattern.compile(pattern);
            Matcher matcher = compiledPattern.matcher(URL);

            if(matcher.find()){
                youtubeCode=matcher.group();
                Log.e("TAG",youtubeCode);
            }
             player.loadVideo(youtubeCode);
            // Hiding player controls
            player.setPlayerStyle(YouTubePlayer.PlayerStyle.DEFAULT);
        }
    }




}





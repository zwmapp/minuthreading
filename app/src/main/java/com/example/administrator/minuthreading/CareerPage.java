package com.example.administrator.minuthreading;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.administrator.minuthreading.Util.Api;
import com.example.administrator.minuthreading.Util.FilePath;
import com.example.administrator.minuthreading.fragments.AppBar;

import net.gotev.uploadservice.MultipartUploadRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class CareerPage extends AppCompatActivity implements View.OnClickListener {

    private EditText et_cfname;
    private EditText et_clname;
    private EditText et_cemail;
    private EditText et_cphone;
    private EditText et_ccomments;
    private Button bt_cbrowse;
    private Button bt_csend;
    private TextView tv_cmsg;
    public String cfname;
    public String clname;
    public String cemail;
    public String cphone;
    public String ccomments;
    private Uri filePath;
    private Integer PICK_PDF_REQUEST=1;
    private static final int STORAGE_PERMISSION_CODE = 123;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_career_page);

        AppBar mainAppBar = (AppBar) getSupportFragmentManager().findFragmentById(R.id.mainAppBar);
        mainAppBar.setAppbarTitle("CAREER ");
        mainAppBar.setBackEnabled(true);
        requestStoragePermission();
        mainAppBar.setBackEnabled(true);
        requestStoragePermission();
        et_cfname=(EditText)findViewById(R.id.career_fname_et);
        et_clname=(EditText)findViewById(R.id.career_lname_et);
        et_cemail=(EditText)findViewById(R.id.career_email_et);
        et_cphone=(EditText)findViewById(R.id.career_phone_et);
        et_ccomments=(EditText)findViewById(R.id.career_comments_et);
        tv_cmsg=(TextView)findViewById(R.id.cmsg_tv);
        bt_cbrowse=(Button)findViewById(R.id.resume_bt);
        bt_cbrowse.setOnClickListener(this);
        bt_csend=(Button)findViewById(R.id.career_send_bt);
        bt_csend.setOnClickListener(this);
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onClick(View v) {

        switch(v.getId()){
            case R.id.resume_bt:

                showFileChooser();
                break;
            case R.id.career_send_bt:
                if(ValidationC()){
                    appointmentSubmit();
                    uploadMultipart();
                }
                break;
        }

    }
    public boolean ValidationC(){
        if( et_cfname.getText().toString().length() == 0 ) {
            Toast.makeText(CareerPage.this,"First Name is required!",Toast.LENGTH_SHORT).show();
            return false;
        }else if( et_clname.getText().toString().length() == 0 ) {
            Toast.makeText(CareerPage.this,"Last name is required!",Toast.LENGTH_SHORT).show();
            return false;
        }else if( et_cphone.getText().toString().length() == 0  ) {
            Toast.makeText(CareerPage.this,"Phone  is required!",Toast.LENGTH_SHORT).show();
            return false;
        }else if( et_cphone.getText().toString().length() <10) {
            Toast.makeText(CareerPage.this,"Phone Number Should be of 10 Digits!",Toast.LENGTH_SHORT).show();
            return false;
        }else if( et_cemail.getText().toString().length() == 0) {
            Toast.makeText(CareerPage.this,"Email is required!",Toast.LENGTH_SHORT).show();
            return false;
        }else if( !et_cemail.getText().toString().matches("[a-zA-Z0-9._-]+@[a-z]+.[a-z]+") ) {
            Toast.makeText(CareerPage.this,"Correct Email Address Required!",Toast.LENGTH_SHORT).show();
            return false;
        }else if((et_ccomments.getText().toString().length() == 0)) {
            Toast.makeText(CareerPage.this,"Please gives some feedback",Toast.LENGTH_SHORT).show();
            return false;
        }else if(filePath==null) {
            Toast.makeText(CareerPage.this,"Resume Required!",Toast.LENGTH_SHORT).show();
            return false;
        }else{
            return true;
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public void appointmentSubmit() {
        cfname = et_cfname.getText().toString();
        clname = et_clname.getText().toString();
        cphone = et_cphone.getText().toString();
        cemail = et_cemail.getText().toString();
        ccomments = et_ccomments.getText().toString();
        final String file= FilePath.getPath(this,filePath);

        StringRequest request = new StringRequest(Request.Method.POST, Api.URL_CAREER, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    JSONObject jobj = new JSONObject(response);

                    String status = jobj.getString("status");
                    String msg = jobj.getString("message");
                    if (status.equalsIgnoreCase("true")){

                        //tv_cmsg.setText("File uploaded successfully");
                       // tv_cmsg.setCompoundDrawablesWithIntrinsicBounds(R.drawable.doubletick, 0, 0, 0);
                        Toast.makeText(CareerPage.this, msg, Toast.LENGTH_SHORT).show();
                        et_cfname.getText().clear();
                        et_clname.getText().clear();
                        et_cphone.getText().clear();
                        et_cemail.getText().clear();
                        et_ccomments.getText().clear();
                        tv_cmsg.setText("No file chosen");

                    }




                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }) {
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("fname",cfname);
                params.put("lname",clname);
                params.put("email",cemail );
                params.put("phone",cphone);
                params.put("comment",ccomments);
                //params.put("file",file);
                Log.e("REQUEST", String.valueOf(params));
                return params;
            }

        };

        RequestQueue requestQueue = Volley.newRequestQueue(CareerPage.this);
        requestQueue.add(request);

    }
    private void showFileChooser() {
        Intent intent = new Intent();
        intent.setType("*/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Pdf"), PICK_PDF_REQUEST);
    }

    //handling the image chooser activity result
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_PDF_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {
            filePath = data.getData();
            String filename = filePath.getLastPathSegment();
            if(filePath != null && !filePath.equals("")){
                tv_cmsg.setText(filename);
                //tv_cmsg.setCompoundDrawablesWithIntrinsicBounds(R.drawable.doubletick, 0, 0, 0);
            }else{

            }


        }
    }
    private void requestStoragePermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED)
            return;

        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
            //If the user has denied the permission previously your code will come to this block
            //Here you can explain why you need this permission
            //Explain here why you need this permission
        }
        //And finally ask for the permission
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, STORAGE_PERMISSION_CODE);
    }


    //This method will be called when the user will tap on allow or deny
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        //Checking the request code of our request
        if (requestCode == STORAGE_PERMISSION_CODE) {

            //If permission is granted
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                //Displaying a toast
                Toast.makeText(this, "Permission granted now you can read the storage", Toast.LENGTH_LONG).show();
            } else {
                //Displaying another toast if permission is not granted
                Toast.makeText(this, "Oops you just denied the permission", Toast.LENGTH_LONG).show();
            }
        }
    }

    public void uploadMultipart() {

        //getting the actual path of the image
        String path = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT) {
            path = FilePath.getPath(this, filePath);
        }

        if (path == null) {

            Toast.makeText(this, "Please move your file to internal storage and retry", Toast.LENGTH_LONG).show();
        } else {
            //Uploading code
            try {

                //Creating a multi part request
                new MultipartUploadRequest(this, Api.URL_APPOINTMENT)
                        .addFileToUpload(path, "file") //Adding file
                        .setMaxRetries(2)
                        .startUpload(); //Starting the upload
                Log.e("File",path);

            } catch (Exception exc) {
                Toast.makeText(this, exc.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }
    }


}

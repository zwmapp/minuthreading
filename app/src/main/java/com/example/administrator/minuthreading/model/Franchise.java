package com.example.administrator.minuthreading.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by amit on 5/21/2018.
 */

public class Franchise {
    @SerializedName("items")
    @Expose
    public String ddescription;

    public Franchise(String ddescription) {
        this.ddescription = ddescription;
    }

    public String getDdescription() {
        return ddescription;
    }

    public void setDdescription(String ddescription) {
        this.ddescription = ddescription;
    }

    @Override
    public String toString() {
        return "Franchise{" +
                "ddescription='" + ddescription + '\'' +
                '}';
    }
}

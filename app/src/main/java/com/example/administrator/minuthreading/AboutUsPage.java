package com.example.administrator.minuthreading;

import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.example.administrator.minuthreading.Util.Api;
import com.example.administrator.minuthreading.fragments.AppBar;
import com.example.administrator.minuthreading.model.AboutUs;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

public class AboutUsPage extends AppCompatActivity {

    private ImageView iv_about;
    private TextView tv_about;
    AboutUs aboutUs=null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about_us_page);

        AppBar mainAppBar = (AppBar) getSupportFragmentManager().findFragmentById(R.id.mainAppBar);
        mainAppBar.setAppbarTitle("ABOUT US ");
        mainAppBar.setBackEnabled(true);

        iv_about=(ImageView)findViewById(R.id.about_iv);
        tv_about=(TextView)findViewById(R.id.about_desc);

        aboutInfo();
    }

    public void aboutInfo(){
        final ProgressDialog loading = ProgressDialog.show(AboutUsPage.this, "", "", false, false);
        JsonObjectRequest jsonObjectRequest= new JsonObjectRequest(Request.Method.POST, Api.URL_ABOUTUS, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                Gson gson=new Gson();
                try {
                    aboutUs=gson.fromJson(response.getString("items"), AboutUs.class);
                    Log.e("TAG", aboutUs.toString());
                    if (aboutUs !=null){
                        init();
                        loading.dismiss();
                    }
                    else {
                        Toast.makeText(AboutUsPage.this,"kuch toh gadbad hai DAYA",Toast.LENGTH_SHORT).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                loading.dismiss();


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });

        RequestQueue requestQueue= Volley.newRequestQueue(AboutUsPage.this);
        requestQueue.add(jsonObjectRequest);

    }

    private void init() {
        Glide.with(AboutUsPage.this).load(aboutUs.about_Url).into(iv_about);
        tv_about.setText(aboutUs.about_data);
    }
}

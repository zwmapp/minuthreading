package com.example.administrator.minuthreading.Adapter;

import android.app.VoiceInteractor;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.administrator.minuthreading.R;
import com.example.administrator.minuthreading.model.Location;

import java.util.ArrayList;

/**
 * Created by amit on 5/11/2018.
 */

public class LocationAdapter extends RecyclerView.Adapter<LocationAdapter.MyViewHolder> {

    Context context;
    ArrayList<Location>locationArrayList;
    public LocationAdapter(Context context,ArrayList<Location>locationArrayList) {
        this.context=context;
        this.locationArrayList=locationArrayList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.location_list, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        holder.title.setText(locationArrayList.get(position).getLocationAddress());
        holder.timing.setText(locationArrayList.get(position).getLocationTimeTable());
        holder.contact.setText(locationArrayList.get(position).getLocationPhone());
    }

    @Override
    public int getItemCount() {
        return locationArrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title, timing, contact;

        public MyViewHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.location_title_tv);
            timing = (TextView) view.findViewById(R.id.location_timing_tv);
            contact = (TextView) view.findViewById(R.id.location_contact_tv);
        }
    }


}

package com.example.administrator.minuthreading.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Administrator on 11/20/2017.
 */

public class ServiceItems {

    @SerializedName("image")
    @Expose
    public String serviceImage;
    @SerializedName("title")
    @Expose
    public String serviceTitle;
    @SerializedName("desc")
    @Expose
    public String serviceDesc;
    @SerializedName("details")
    @Expose
    public String serviceDetails;

    @Override
    public String toString() {
        return "ServiceItems{" +
                "serviceImage='" + serviceImage + '\'' +
                ", serviceTitle='" + serviceTitle + '\'' +
                ", serviceDesc='" + serviceDesc + '\'' +
                ", serviceDetails='" + serviceDetails + '\'' +
                '}';
    }



    public ServiceItems(String serviceImage, String serviceTitle, String serviceDesc, String serviceDetails) {
        this.serviceImage = serviceImage;
        this.serviceTitle = serviceTitle;
        this.serviceDesc = serviceDesc;
        this.serviceDetails = serviceDetails;
    }



}

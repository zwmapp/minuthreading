package com.example.administrator.minuthreading.Adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.administrator.minuthreading.R;
import com.example.administrator.minuthreading.ServicesPage;
import com.example.administrator.minuthreading.model.Service;
import com.example.administrator.minuthreading.model.ServiceItems;

import java.util.ArrayList;

import javax.microedition.khronos.opengles.GL;

import static android.content.Context.LAYOUT_INFLATER_SERVICE;

/**
 * Created by Administrator on 11/20/2017.
 */

public class ServiceAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    ViewHolder viewHolder;
    Service service;
    ArrayList<ServiceItems> allservices;
    Context context;
    AlertDialog.Builder builder;

    public ServiceAdapter(ServicesPage servicesPage, ArrayList<ServiceItems> allservices) {

        this.context = servicesPage;
        this.allservices = allservices;
        builder = null;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.service_card_items, parent, false);

        viewHolder = new ViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {

        viewHolder.tv_title.setText(Html.fromHtml(allservices.get(position).serviceTitle));
        viewHolder.tv_desc.setText(Html.fromHtml(allservices.get(position).serviceDesc));

        new Thread(new Runnable() {
            @Override
            public void run() {
                AppCompatActivity activity=(AppCompatActivity)context;
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Glide.with(context).load(allservices.get(position).serviceImage).into(viewHolder.iv_image);
                    }
                });

            }
        }).start();

        viewHolder.bt_readmore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showProdutoDialog(context,position);

            }
        });


    }

    @Override
    public int getItemCount() {
        return allservices.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView tv_title;
        TextView tv_desc;
        ImageView iv_image;
        Button bt_readmore;

        ViewHolder(View view) {
            super(view);
            tv_title = (TextView) view.findViewById(R.id.serviceTitle_tv);
            tv_desc = (TextView) view.findViewById(R.id.serviceDesc_tv);
            iv_image = (ImageView) view.findViewById(R.id.serviceImage_iv);
            bt_readmore = (Button) view.findViewById(R.id.serviceRead_bt);
        }
    }

    public void showProdutoDialog(final Context context, int position) {
        builder = new AlertDialog.Builder(context);
        LayoutInflater inf = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inf.inflate(R.layout.service_pop_msg, null);
        builder.setCancelable(false);
        builder.setView(view);
        TextView tv_contentTitle = view.findViewById(R.id.itemttile_tv);
        tv_contentTitle.setText(Html.fromHtml(allservices.get(position).serviceTitle));
        TextView tv_contentDesc = view.findViewById(R.id.serviceContent_tv);
        tv_contentDesc.setText(Html.fromHtml(allservices.get(position).serviceDetails));

        builder.setNegativeButton(" Close ", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        builder.show();

    }


}

package com.example.administrator.minuthreading.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Administrator on 11/20/2017.
 */

public class Price {

    @SerializedName("name")
    @Expose
    public String title;


    @SerializedName("pricelist")
    @Expose
    public  ArrayList<ArrayList<String>> priceitems;


    public Price(String title, ArrayList<ArrayList<String>> priceitems) {
        this.title = title;
        this.priceitems = priceitems;
    }


    @Override
    public String toString() {
        return "Price{" +
                "title='" + title + '\'' +
                ", price=" + priceitems +
                '}';
    }




}

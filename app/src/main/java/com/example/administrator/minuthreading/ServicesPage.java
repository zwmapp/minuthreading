package com.example.administrator.minuthreading;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.administrator.minuthreading.Adapter.ServiceAdapter;
import com.example.administrator.minuthreading.Util.Api;
import com.example.administrator.minuthreading.fragments.AppBar;
import com.example.administrator.minuthreading.model.Service;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;


import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;

public class ServicesPage extends AppCompatActivity {
    Service service=null;
    private TextView servicemsg;
    RecyclerView service_rv;
    ServiceAdapter adapter=null;
    private RecyclerView.LayoutManager layoutManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_serivices_page);

        servicemsg=(TextView)findViewById(R.id.servicemsg_tv);


        AppBar mainAppBar = (AppBar) getSupportFragmentManager().findFragmentById(R.id.mainAppBar);
        mainAppBar.setAppbarTitle("OUR SERVICES ");
        mainAppBar.setBackEnabled(true);


        serviceInfo();
    }

    public  void serviceInfo(){

        JsonObjectRequest request=new JsonObjectRequest(Request.Method.POST, Api.URL_SERVICESLIST, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Gson gson=new Gson();
                Type type=new TypeToken<Service>(){}.getType();
                try {

                    service=gson.fromJson(response.getString("items"),type);

                    } catch (JSONException e) {
                    e.printStackTrace();
                }
                init();
                Log.e("SERVICES",service.toString());
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        RequestQueue requestQueue = Volley.newRequestQueue(ServicesPage.this);
        requestQueue.add(request);
    }

    public void init(){

        servicemsg.setText(service.serviceMsg);

        service_rv=(RecyclerView)findViewById(R.id.service_rv) ;
        layoutManager = new LinearLayoutManager(this);
        service_rv.setLayoutManager(layoutManager);
        service_rv.setAdapter(new ServiceAdapter(this,service.allservices));

    }

}

package com.example.administrator.minuthreading.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Administrator on 11/17/2017.
 */

public class Testimonial {

    @SerializedName("image")
    @Expose
    public String imageURl;

    @SerializedName("desc")
    @Expose
    public String testDescp;

    public Testimonial() {
    }

    public Testimonial(String imageURl, String testDescp) {
        this.imageURl = imageURl;
        this.testDescp = testDescp;
    }

    @Override
    public String toString() {
        return "Testimonial{" +
                "imageURl='" + imageURl + '\'' +
                ", testDescp='" + testDescp + '\'' +
                '}';
    }


}

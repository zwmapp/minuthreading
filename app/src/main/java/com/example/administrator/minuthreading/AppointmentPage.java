package com.example.administrator.minuthreading;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.administrator.minuthreading.Util.Api;
import com.example.administrator.minuthreading.Util.FilePath;
import com.example.administrator.minuthreading.fragments.AppBar;

import net.gotev.uploadservice.MultipartUploadRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class AppointmentPage extends AppCompatActivity implements View.OnClickListener{

    private EditText et_fname;
    private EditText et_lname;
    private EditText et_email;
    private EditText et_phone;
    private EditText et_comments;
    private Button bt_browse;
    private Button bt_send;
    private TextView tv_msg;
    public String fname;
    public String lname;
    public String email;
    public String phone;
    public String comments;
    private Integer PICK_PDF_REQUEST=1;
    private Uri filePath;
    private static final int STORAGE_PERMISSION_CODE = 123;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_appointment_page);

        AppBar mainAppBar = (AppBar) getSupportFragmentManager().findFragmentById(R.id.mainAppBar);
        mainAppBar.setAppbarTitle("MAKE AN APPOINTMENT");
        mainAppBar.setBackEnabled(true);
        requestStoragePermission();
        et_fname=(EditText)findViewById(R.id.appointment_fname_et);
        et_lname=(EditText)findViewById(R.id.appointment_lname_et);
        et_email=(EditText)findViewById(R.id.appointment_email_et);
        et_phone=(EditText)findViewById(R.id.appointment_phone_et);
        et_comments=(EditText)findViewById(R.id.appointment_comment_et);
        tv_msg=(TextView)findViewById(R.id.file_tv);

        bt_browse=(Button)findViewById(R.id.appointment_browse_bt);
        bt_browse.setOnClickListener(this);
        bt_send=(Button)findViewById(R.id.appointment_send_bt);
        bt_send.setOnClickListener(this);

    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onClick(View v) {
         switch(v.getId()){
             case R.id.appointment_browse_bt:

                    showFileChooser();

                 break;
             case R.id.appointment_send_bt:
                 if(Validation()){
                     appointmentSubmit();
                     if(filePath!=null) {
                         uploadMultipart();
                     }
                 }
                 break;
         }
    }

    public boolean Validation(){
            if( et_fname.getText().toString().length() == 0 ) {
                Toast.makeText(AppointmentPage.this,"First Name is required!",Toast.LENGTH_SHORT).show();
                return false;
            }else if( et_lname.getText().toString().length() == 0 ) {
                Toast.makeText(AppointmentPage.this,"Last name is required!",Toast.LENGTH_SHORT).show();
                return false;
            }else if( et_phone.getText().toString().length() == 0  ) {
                Toast.makeText(AppointmentPage.this,"Phone  is required!",Toast.LENGTH_SHORT).show();
                return false;
            }else if( et_phone.getText().toString().length() <10) {
                Toast.makeText(AppointmentPage.this,"Phone Number Should be of 10 Digits!",Toast.LENGTH_SHORT).show();
                return false;
            }else if( et_email.getText().toString().length() == 0) {
                Toast.makeText(AppointmentPage.this,"Email is required!",Toast.LENGTH_SHORT).show();
                return false;
            }else if( !et_email.getText().toString().matches("[a-zA-Z0-9._-]+@[a-z]+.[a-z]+") ) {
                Toast.makeText(AppointmentPage.this,"Correct Email Address Required!",Toast.LENGTH_SHORT).show();
                return false;

            }else if((et_comments.getText().toString().length() == 0)) {
                Toast.makeText(AppointmentPage.this,"Please gives some feedback",Toast.LENGTH_SHORT).show();
                return false;
            }else{
                return true;
            }
        }

        @RequiresApi(api = Build.VERSION_CODES.KITKAT)
        public void appointmentSubmit() {
            fname = et_fname.getText().toString();
            lname = et_lname.getText().toString();
            phone = et_phone.getText().toString();
            email = et_email.getText().toString();
            comments = et_comments.getText().toString();


            StringRequest request = new StringRequest(Request.Method.POST, Api.URL_APPOINTMENT, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {

                    try {
                        JSONObject jobj = new JSONObject(response);

                        String status = jobj.getString("status");

                        String msg = jobj.getString("message");
                        if (status.equalsIgnoreCase("true")){

                            Toast.makeText(AppointmentPage.this,msg, Toast.LENGTH_SHORT).show();
                            et_fname.getText().clear();
                            et_lname.getText().clear();
                            et_phone.getText().clear();
                            et_email.getText().clear();
                            et_comments.getText().clear();
                            tv_msg.setText("No file chosen");
                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                }
            }) {
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<>();
                    params.put("fname",fname);
                    params.put("lname",lname);
                    params.put("email",email );
                    params.put("phone",phone);
                    params.put("comment",comments);
                   // params.put("file",filepath);
                    Log.e("REQUEST", String.valueOf(params));
                    return params;
                }



            };

                RequestQueue requestQueue = Volley.newRequestQueue(AppointmentPage.this);
                requestQueue.add(request);

            }

    //method to show file chooser
    private void showFileChooser() {
        Intent intent = new Intent();
        intent.setType("*/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select File"), PICK_PDF_REQUEST);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_PDF_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {

            filePath = data.getData();
            String filename = filePath.getLastPathSegment();
            tv_msg.setText(filename);
        }
    }

    //Requesting permission
    private void requestStoragePermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED)
            return;

        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
            //If the user has denied the permission previously your code will come to this block
            //Here you can explain why you need this permission
            //Explain here why you need this permission
        }
        //And finally ask for the permission
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, STORAGE_PERMISSION_CODE);
    }


    //This method will be called when the user will tap on allow or deny
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        //Checking the request code of our request
        if (requestCode == STORAGE_PERMISSION_CODE) {

            //If permission is granted
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                //Displaying a toast
                Toast.makeText(this, "Permission granted now you can read the storage", Toast.LENGTH_LONG).show();
            } else {
                //Displaying another toast if permission is not granted
                Toast.makeText(this, "Oops you just denied the permission", Toast.LENGTH_LONG).show();
            }
        }
    }

    public void uploadMultipart() {

        //getting the actual path of the image
        String path = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT) {
            path = FilePath.getPath(this, filePath);
        }

        if (path == null) {

            Toast.makeText(this, "Please move your file to internal storage and retry", Toast.LENGTH_LONG).show();
        } else {
            //Uploading code
            try {

                //Creating a multi part request
                new MultipartUploadRequest(this, Api.URL_APPOINTMENT)
                        .addFileToUpload(path, "file") //Adding file
                        .setMaxRetries(2)
                        .startUpload(); //Starting the upload
                Log.e("File",path);

            } catch (Exception exc) {
                Toast.makeText(this, exc.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }
    }





}


